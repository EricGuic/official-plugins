# PeerTube video annotation

Add a field in the video form so users can set annotation to their video.

![](./screens/player.png)
![](./screens/config.png)

By default, the annotation will be on the top-right of the player.

Annotations format:

```
start --> stop
options: align=top-left (or top, top-right, right, bottom-right, bottom, bottom-left, left)
HTML
```

  * `start` (in seconds): When to show the annotation
  * `stop` (in seconds): When to hide the annotation
  * `options: ...` (this line is optional): Set options for your annotation
  * `HTML`: Content of your annotation

For example:

```
 --> 4
Hello, how are you?

5-->10
See <a href="https://cpy.re" target="_blank">this document</a> for more information

12-->
options: align=top-left
This annotation will be at the top-left of the player

```

 * In the first section, the text will be hidden after 4 seconds
 * In the second section, the text will be displayed at second 5 and will be hidden at second 10
 * In the third section, the text will be displayed at second 12 but will not be hidden. The text will also be located at the top left of the video.
